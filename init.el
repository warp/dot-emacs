(load-file (locate-user-emacs-file "lisp/util.el"))
(load-file (locate-user-emacs-file "lisp/macros.el"))
(load-file (locate-user-emacs-file "lisp/globals.el"))
(load-file (locate-user-emacs-file "lisp/indent.el"))

(define-key emacs-lisp-mode-map (kbd "C-c C-l") 'load-file)

;; bootstrap straight.el
(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

;; packages begin

(straight-use-package 'use-package)
(eval-when-compile (require 'use-package))

;; TODO: migrate to `general.el' for keybindings

(use-package s :straight t)

(use-package evil :straight t :demand t
  :init
  (setq evil-respect-visual-line-mode t)
  :config
  (evil-define-key 'motion 'global
    (kbd "RET") nil
    (kbd "TAB") nil))

(use-package lispy :straight t
  :commands lispy-define-key
  :hook ((emacs-lisp-mode . lispy-mode)
         (lisp-mode . lispy-mode)
         (scheme-mode . lispy-mode)
         (geiser-repl-mode . lispy-mode)))

(use-package magit :straight t
  :bind (("C-x g" . magit-status)
         ("C-x G" . magit-dispatch)
         ("C-x M-g" . magit-clone))
  :config
  (magit-add-section-hook
   'magit-status-sections-hook 'magit-insert-tracked-files nil t))

(use-package lispyville :straight t :after lispy
  :hook (lispy-mode . lispyville-mode)
  :config
  (lispyville-set-key-theme
   '(operators
     c-w
     prettify
     additional
     additional-motions
     additional-insert
     commentary
     slurp/barf-cp
     escape))
  :config
  (lispy-define-key lispy-mode-map "M-v" #'lispy-mark-symbol)
  (lispy-define-key lispy-mode-map "v" #'lispyville-toggle-mark-type))

(use-package org :straight t
  :bind ("C-c a" . org-agenda)
  :config
  (evil-define-key '(normal motion) org-agenda-mode-map
    "t" 'org-agenda-todo))

(use-package ledger-mode :straight t
  :hook (ledger-mode . (lambda () (setq-local tab-always-indent 'complete))))

(use-package markdown-mode :straight t)

(use-package geiser :straight t :after scheme-mode)

(use-package visual-fill-column :straight t
  :hook (visual-line-mode . visual-fill-column-mode))

(use-package adaptive-wrap :straight t
  :hook (visual-line-mode . adaptive-wrap-prefix-mode))

(use-package writeroom-mode :straight t
  :bind ("C-c w" . writeroom-mode)
  :config (setq writeroom-width fill-column))

(use-package auto-capitalize
  :straight (:host github :repo "yuutayamada/auto-capitalize-el")
  :hook (after-change-major-mode . auto-capitalize-mode))

(use-package make-it-so :straight t
  :after dired
  :init
  (mis-config-default)
  (evil-define-key 'normal dired-mode-map
    "," 'make-it-so))

(use-package golden-ratio :straight t
  :demand t
  :config (golden-ratio-mode 1))

(use-package tex :defer t :straight auctex)

(use-package posframe :straight t)

(use-package paren-face :straight t)

(use-package nov :straight t :mode ("\\.epub\\'" . nov-mode))

(use-package rebecca-theme :straight t :defer t :if window-system)
(use-package solarized-theme :straight t :defer t :if window-system)

(use-package circadian :straight t :defer 1 :if window-system
  :config (circadian-setup))

(defun pdf-tools-colorize ()
  (setq pdf-view-midnight-colors
        (cons (face-foreground 'default)
              (face-background 'default)))
  (pdf-view-midnight-minor-mode 1))

(use-package pdf-tools :straight t :if window-system :defer 1
  :init (pdf-loader-install)
  :hook (pdf-view-mode . pdf-tools-colorize))

(use-package leaves
  :straight (:local-repo "~/dev/lisp/leaves"))
(use-package leaves-sync
  :straight (:local-repo "~/dev/lisp/leaves-sync"))
(use-package depub
  :straight (:local-repo "~/dev/lisp/depub"))

;; packages end

(load-file (locate-user-emacs-file "lisp/colemak.el"))

(setq org-mode-hook nil)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(TeX-auto-save t)
 '(TeX-parse-self t)
 '(blink-cursor-mode nil)
 '(calendar-location-name "New York City")
 '(circadian-themes (quote (("7:30" . solarized-light) ("17:00" . rebecca))))
 '(confirm-kill-processes nil)
 '(custom-enabled-themes (quote (solarized-light)))
 '(custom-safe-themes t)
 '(default-frame-alist (quote ((width . 90) (height . 35))))
 '(default-input-method "TeX")
 '(display-battery-mode t)
 '(enable-recursive-minibuffers t)
 '(evil-emacs-state-modes
   (quote
    (help-mode tooltip-mode haskell-interactive-mode magit-mode pdf-view-mode geiser-repl-mode ediff-mode tablist-mode)))
 '(evil-highlight-closing-paren-at-point-states (quote (not emacs insert replace lispy)))
 '(evil-mode t)
 '(evil-want-fine-undo t)
 '(evil-want-minibuffer t)
 '(fill-column 80)
 '(geiser-guile-load-path (quote ("/home/parsley/dev/scheme")))
 '(global-paren-face-mode t)
 '(global-prettify-symbols-mode t)
 '(global-undo-tree-mode t)
 '(icomplete-mode t)
 '(indent-tabs-mode nil)
 '(indicate-empty-lines t)
 '(inhibit-startup-screen t)
 '(ledger-reconcile-buffer-line-format
   "%(date)s %-4(code)s %-25(payee)s %-10(account)s %10(amount)s
")
 '(ledger-reconcile-buffer-payee-max-chars 25)
 '(line-number-mode t)
 '(lispy-close-quotes-at-end-p t)
 '(lispyville-commands-put-into-special t)
 '(markdown-asymmetric-header t)
 '(menu-bar-mode nil)
 '(minibuffer-depth-indicate-mode t)
 '(mis-recipes-directory "~/dev/mis")
 '(org-adapt-indentation nil)
 '(org-agenda-files "~/.agenda_files")
 '(org-agenda-prefix-format
   (quote
    ((agenda . " %i %-15:c%?-15t% s")
     (todo . " %i %-12:c")
     (tags . " %i %-12:c")
     (search . " %i %-12:c"))))
 '(org-agenda-restore-windows-after-quit t)
 '(org-agenda-todo-keyword-format "%-4s")
 '(org-agenda-window-setup (quote only-window))
 '(org-babel-load-languages (quote ((emacs-lisp . t) (ledger . t))))
 '(org-clock-idle-time 15)
 '(org-export-backends (quote (ascii html latex md odt)))
 '(org-export-with-section-numbers nil)
 '(org-footnote-auto-adjust t)
 '(org-link-descriptive nil)
 '(org-link-doi-server-url "http://sci-hub.tw/")
 '(org-log-repeat (quote note))
 '(org-src-preserve-indentation t)
 '(package-enable-at-startup nil)
 '(pdf-view-midnight-colors (quote ("#282828" . "#f2e5bc")))
 '(ring-bell-function (quote ignore))
 '(scroll-bar-mode nil)
 '(scroll-preserve-screen-position 1)
 '(sentence-end-double-space nil)
 '(show-paren-mode t)
 '(split-width-threshold 100)
 '(straight-use-package-by-default t)
 '(tool-bar-mode nil)
 '(truncate-lines t)
 '(view-read-only t)
 '(writeroom-mode-line
   (quote
    ("   " mode-line-modified "   " mode-line-buffer-identification)))
 '(writeroom-width 80))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(fixed-pitch ((t nil)))
 '(fixed-pitch-serif ((t nil)))
 '(hydra-face-red ((t (:inherit font-lock-warning-face))))
 '(parenthesis ((t (:inherit shadow)))))
