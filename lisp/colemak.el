;;; colemak.el --- Colemak key bindings for (evil) emacs

(evil-define-key '(normal visual motion) 'global
  "n" 'evil-next-line
  "N" 'evil-join
  "gn" 'evil-next-visual-line
  "gN" 'evil-join-whitespace
  "p" 'evil-previous-line
  "gp" 'evil-previous-visual-line
  "j" 'evil-search-next
  "J" 'evil-search-previous
  "gj" 'evil-next-match
  "gJ" 'evil-previous-match
  "k" 'evil-paste-after
  "K" 'evil-paste-before
  (kbd "C-k") 'evil-paste-pop)

(lispy-define-key lispy-mode-map "n" 'special-lispy-down)
(lispy-define-key lispy-mode-map "N" 'special-lispy-outline-next)
(lispy-define-key lispy-mode-map "p" 'special-lispy-up)
(lispy-define-key lispy-mode-map "P" 'special-lispy-outline-prev)
(evil-define-key '(normal motion visual) lispyville-mode-map (kbd "M-n") 'lispyville-move-down)
(evil-define-key '(normal motion visual) lispyville-mode-map (kbd "M-p") 'lispyville-move-up)
