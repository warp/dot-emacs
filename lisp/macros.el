;;; macros.el --- my commonly used keyboard macros

;; "Demote" the next occurrence of emphasized text in markdown-mode.
;; Call multiple times to demote the same phrase (e.g. call twice to remove bold
;; emphasis).
(fset 'md-unemphasize [?\C-s ?* ?\C-c ?\C-s ?i])
