;;; indent.el --- indentation rules for lisp and scheme

(put 'apply 'lisp-indent-function 1)
(put 'funcall 'lisp-indent-function 1)
(put 'mapcar 'lisp-indent-function 1)
(put 'mapcan 'lisp-indent-function 1)
(put 'mapconcat 'lisp-indent-function 1)
(put 'cl-mapcar 'lisp-indent-function 1)
(put 'add-hook 'lisp-indent-function 'defun)
(put 'format 'lisp-indent-function 1)
(put 'completing-read 'lisp-indent-function 1)
(put 'completing-read-multiple 'lisp-indent-function 1)

(put 'format 'scheme-indent-function 2)
(put 'fresh 'scheme-indent-function 'defun)
(put 'set-record-type-printer! 'scheme-indent-function 'defun)
