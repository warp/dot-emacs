;;; util.el --- simple utility functions I use often

(defconst util-file-name load-file-name "path to util file")

(defun quick-init ()
  (interactive)
  (find-file user-init-file))

(defun quick-util ()
  (interactive)
  (find-file util-file-name))

(defun quick-messages ()
  (interactive)
  (switch-to-buffer (messages-buffer)))

(defun quick-scratch (&optional split)
   "go to scratch buffer, creating one if necessary"
   (interactive "P")
   (switch-to-buffer (get-buffer-create "*scratch*"))
   (lisp-interaction-mode))

(defun unfill-column ()
  (interactive)
  (let ((fill-column (point-max)))
    (call-interactively #'fill-individual-paragraphs)))

;;; util.el ends here
